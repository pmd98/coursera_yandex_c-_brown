#pragma once
#ifndef RequestManager_h
#define RequestManager_h

#include <cstdint>
#include <exception>
#include <iostream>
#include <iterator>
#include <memory>
#include <optional>
#include <sstream>
#include <string>
#include <system_error>
#include <type_traits>
#include <unordered_map>
#include <vector>
#include "BusManager.h"
#include"parse.h"

struct Request;
using RequestHolder = std::unique_ptr<Request>;



struct Request {
    enum class Type {
        BUS,
        STOP
    };
    Request(Type type) : type(type) {}
    static RequestHolder Create(Type type);
    virtual void ParseFrom(std::string_view input) = 0;
    virtual ~Request() = default;

    const Type type;
};

const std::unordered_map<std::string_view, Request::Type> STR_TO_REQUEST_TYPE = {
    {"Bus", Request::Type::BUS},
    {"Stop", Request::Type::STOP}
};

template <typename ResultType>
struct ReadRequest : Request {
    
    using Request::Request;
    virtual ResultType Process(const BusManager& manager) const = 0;
};


struct BusRequest : ReadRequest<std::string> {
    BusRequest() : ReadRequest(Type::BUS) {}
    void ParseFrom(std::string_view input) override {
        bus_name = input;
    }

    std::string Process(const BusManager& manager) const override {
        if (manager.BusContain(bus_name)) {
            const auto& route = manager.GetRoute(bus_name);
            std::ostringstream result;
            result << "Bus " << bus_name << ": "<< 
                route.stop_count << " stops on route, " <<
                route.unique_stops.size() << " unique stops, " <<
                route.path << " route length, " <<
                route.curvature << " curvature";
            return result.str();
        }
        return std::string("Bus " + bus_name +": not found");
    }

    std::string bus_name;
};

struct StopRequest : ReadRequest<std::string> {
    StopRequest() : ReadRequest(Type::STOP) {}
    void ParseFrom(std::string_view input) override {
        stop_name = input;
    }

    std::string Process(const BusManager& manager) const override {
        if (manager.StopContain(stop_name)) {
            const auto& buses_on_stop = manager.GetBusesOnStop(stop_name);
            if (!buses_on_stop.empty()) {
                std::ostringstream result;
                result << "Stop " << stop_name << ": buses"; 
                for (const std::string& bus : buses_on_stop) {
                    result << " " << bus;
                }
                return result.str();
            }
            return std::string("Stop " + stop_name + ": no buses");
        }
        return std::string("Stop " + stop_name + ": not found");
    }

    std::string stop_name;
};

RequestHolder Request::Create(Request::Type type) {
    switch (type) {
    case Request::Type::BUS:
        return std::make_unique<BusRequest>();
    case Request::Type::STOP:
        return std::make_unique<StopRequest>();
        return nullptr;
    }
}

template <typename Number>
Number ReadNumberOnLine(std::istream& stream) {
    Number number;
    stream >> number;
    std::string dummy;
    std::getline(stream, dummy);
    return number;
}

std::optional<Request::Type> ConvertRequestTypeFromString(std::string_view type_str) {
    if (const auto it = STR_TO_REQUEST_TYPE.find(type_str);
        it != STR_TO_REQUEST_TYPE.end()) {
        return it->second;
    }
    else {
        return std::nullopt;
    }
}

RequestHolder ParseRequest(std::string_view request_str) {
    const auto request_type = ConvertRequestTypeFromString(ReadToken(request_str));
    if (!request_type) {
        return nullptr;
    }
    RequestHolder request = Request::Create(*request_type);
    if (request) {
        request->ParseFrom(request_str);
    };
    return request;
}

std::vector<RequestHolder> ReadRequests(std::istream& in_stream = std::cin) {
    const size_t request_count = ReadNumberOnLine<size_t>(in_stream);

    std::vector<RequestHolder> requests;
    requests.reserve(request_count);

    for (size_t i = 0; i < request_count; ++i) {
        std::string request_str;
        std::getline(in_stream, request_str);
        if (auto request = ParseRequest(request_str)) {
            requests.push_back(std::move(request));
        }
    }
    return requests;
}

std::vector<std::string> ProcessRequests(const BusManager& manager, const std::vector<RequestHolder>& requests) {
    std::vector<std::string> responses;
    for (const auto& request_holder : requests) {
        const auto& request = static_cast<const BusRequest&>(*request_holder);
        responses.push_back(request.Process(manager));
    }
    return responses;
}

void PrintResponses(const std::vector<std::string>& responses, std::ostream& stream = std::cout) {
    for (const std::string& response : responses) {
        stream << response << "\n";
    }
}

#endif