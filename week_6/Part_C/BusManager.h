#pragma once
#ifndef BusManager_h
#define BusManager_h

#include <cmath>
#include <cstdint>
#include <ctime>
#include <exception>
#include <iostream>
#include <iterator>
#include <memory>
#include <optional>
#include <sstream>
#include <string>
#include <system_error>
#include <type_traits>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#include <algorithm>
#include <set>

//using namespace std;

template<typename It>
class Range {
public:
    Range(It begin, It end) : begin_(begin), end_(end) {}
    It begin() const { return begin_; }
    It end() const { return end_; }

private:
    It begin_;
    It end_;
};

class BusManager {
private:
    struct Stop {
        std::unordered_map<std::string, size_t> path;
        std::set<std::string> buses_on_stop;
        double latitude, longitude;
    };

    struct Route {
        double curvature;
        size_t path;
        double perfect_path;
        size_t stop_count;
        bool cycle;
        std::unordered_set<std::string> unique_stops;
        std::vector<std::string> route;
    };

    std::unordered_map<std::string, Stop> stops;
    std::unordered_map<std::string, Route> buses;

    void ComputeRouteLenght(Route& route) {
        
        for (size_t i = 1; i < route.route.size(); ++i) {
            route.path += ComputeRealPath(route.route[i - 1], route.route[i]);
            route.perfect_path += ComputePath(route.route[i - 1], route.route[i]);
        }
        if (!route.cycle) {
            route.perfect_path *= 2;
            for (size_t i = route.route.size() - 1; i > 0; --i) {
                route.path += ComputeRealPath(route.route[i], route.route[i-1]);
            }
            //route.path *= 2;
        }
    }

    size_t ComputeRealPath(const std::string& lhs_stop, const std::string& rhs_stop) const {
        auto lhs_stop_iter = stops.at(lhs_stop);
        auto lhs_iter = lhs_stop_iter.path.find(rhs_stop);
        if (lhs_iter == lhs_stop_iter.path.end()) {
            return stops.at(rhs_stop).path.find(lhs_stop)->second;
        }
        return lhs_iter->second;
    }

    double ComputePath(const std::string& lhs_stop, const std::string& rhs_stop) const {
        constexpr double PI = 3.1415926535;
        constexpr double RADIUS = 6'371'000;
        double lhs_longtitude = stops.at(lhs_stop).longitude * PI / 180.0;
        double lhs_latitude = stops.at(lhs_stop).latitude * PI / 180.0;

        double rhs_longtitude = stops.at(rhs_stop).longitude * PI / 180.0;
        double rhs_latitude = stops.at(rhs_stop).latitude * PI / 180.0;

        return RADIUS * std::asin(
            std::sqrt(
                std::pow(std::sin(rhs_latitude - lhs_latitude), 2.0) +
                std::cos(lhs_latitude) *
                std::cos(rhs_latitude) *
                std::pow(std::sin(rhs_longtitude - lhs_longtitude), 2.0)
            )
        );
    }

public:
    void AddStop(const std::string& name, double latitude, double longitude,
        const std::unordered_map<std::string,size_t> path) {
        auto& pos = stops[name];
        pos.latitude = latitude;
        pos.longitude = longitude;
        pos.path = path;
    }

    void AddBus(const std::string& bus_name,const std::vector<std::string>& route, bool cycle) {
        auto& pos = buses[bus_name];
        pos.cycle = cycle;
        pos.route.reserve(route.size());
        for (const auto& stop : route) {
            stops[stop].buses_on_stop.insert(bus_name);
            pos.route.push_back(stop);
            pos.unique_stops.insert(stop);
        }
        pos.stop_count = pos.route.size();
        if (!pos.cycle) {
            pos.stop_count *= 2;
            --pos.stop_count;
        }
    }

    void ComputeRouteLenght() {
        for (auto& bus : buses) {
            ComputeRouteLenght(bus.second);
            bus.second.curvature = (bus.second.path * 1.0) / bus.second.perfect_path;
        }
    }

    bool BusContain(const std::string& bus_name) const {
        if (auto iter = buses.find(bus_name); iter != buses.end())
            return true;
        return false;
    }

    bool StopContain(const std::string& stop_name) const {
        if (auto iter = stops.find(stop_name); iter != stops.end())
            return true;
        return false;
    }



    const Route& GetRoute(const std::string& bus_name) const {
        return  buses.at(bus_name);
    }

    const auto& GetBusesOnStop(const std::string& stop_name) const{
        return stops.at(stop_name).buses_on_stop;
    }

};



#endif
