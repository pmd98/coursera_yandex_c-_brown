#pragma once
#ifndef RefreshManager_h
#define RefreshManager_h

#include"RequestManger.h"
#include"parse.h"

struct ModifyRequest;
using ModifyRequestHolder = std::unique_ptr<ModifyRequest>;

struct ModifyRequest {
    enum class Type {
        BUS,
        STOP
    };

    ModifyRequest(Type type) : type(type) {}
    static ModifyRequestHolder Create(Type type);
    virtual void ParseFrom(std::string_view input) = 0;
    virtual void ParseFrom(Json::Node& node) = 0;
    virtual void Process(BusManager& manager) const = 0;
    virtual ~ModifyRequest() = default;
    const Type type;
};

const std::unordered_map<std::string_view, ModifyRequest::Type> STR_TO_MODIFY_REQUEST_TYPE = {
    {"Bus", ModifyRequest::Type::BUS},
    {"Stop", ModifyRequest::Type::STOP}
};

char FindDelimiter(std::string_view input,
    char delimiter_1,
    char delimiter_2) {
    for (const auto elem : input)
    {
        if (delimiter_1 == elem)
            return delimiter_1;
        if (delimiter_2 == elem)
            return delimiter_2;
    }
    return 0;
}

struct BusModifyRequest : ModifyRequest {
    BusModifyRequest() : ModifyRequest(Type::BUS) {}
    void ParseFrom(std::string_view input) override {
        std::string_view delimiter;
        bus_name = ReadToken(input, ":");
        
        if (FindDelimiter(input, '>', '-') == '-') {
            delimiter = "-";
            cycle = false;
        }
        else {
            delimiter = ">";
            cycle = true;
        }
        while (true) {
            auto token = Strip(ReadToken(input, delimiter));
            if (token.empty())
                break;
            route.emplace_back(token);
        }
    }

    void ParseFrom(Json::Node& node) override {
        std::string_view delimiter;
        bus_name = node.AsMap().find("name")->second.AsString();
        cycle = ConvertToBool(node.AsMap().find("is_roundtrip")->second.AsString());
        auto& stops = node.AsMap().find("stops")->second.AsArray();
        for (size_t i = 0; i < stops.size(); ++i) {
            route.push_back(stops[i].AsString());
        }
    }

    void Process(BusManager& manager) const override {
        manager.AddBus(bus_name, route, cycle);
    }

    bool cycle = false;
    std::string bus_name;
    std::vector<std::string> route;
};


struct StopModifyRequest : ModifyRequest {
    StopModifyRequest() : ModifyRequest(Type::BUS) {}
    void ParseFrom(std::string_view input) override {
        stop_name = Strip( ReadToken(input, ":"));
        latitude = ConvertToDouble(Strip(ReadToken(input, ",")));
        longitude = ConvertToDouble(Strip(ReadToken(input, ",")));
        while (true) {
            auto token = Strip(ReadToken(input, "m to "));
            if (token.empty())
                break;
            size_t lenght = ConvertToInt(token);
            token = Strip(ReadToken(input,","));
            path[std::string(token)] = lenght;
        }
    }

    void ParseFrom(Json::Node& node) override {
        std::string_view delimiter;
        stop_name = node.AsMap().find("name")->second.AsString();
        latitude = ConvertToDouble(node.AsMap().find("latitude")->second.AsString());
        longitude = ConvertToDouble(node.AsMap().find("longitude")->second.AsString());
        auto& road_distances = node.AsMap().find("road_distances")->second.AsMap();
        for (auto& distance : road_distances) {
            path[distance.first] = ConvertToInt(distance.second.AsString());
        }
    }

    void Process(BusManager& manager) const override {
        manager.AddStop(stop_name,latitude,longitude, path);
    }

    std::string stop_name;
    double latitude = 0.0, longitude = 0.0;
    std::unordered_map<std::string, size_t> path;
};

ModifyRequestHolder ModifyRequest::Create(ModifyRequest::Type type) {
    switch (type) {
    case ModifyRequest::Type::BUS:
        return std::make_unique<BusModifyRequest>();
    case ModifyRequest::Type::STOP:
        return std::make_unique<StopModifyRequest>();
        return nullptr;
    }
}


std::optional<ModifyRequest::Type> ConvertModifyRequestTypeFromString(std::string_view type_str) {
    if (const auto it = STR_TO_MODIFY_REQUEST_TYPE.find(type_str);
        it != STR_TO_MODIFY_REQUEST_TYPE.end()) {
        return it->second;
    }
    else {
        return std::nullopt;
    }
}


ModifyRequestHolder ParseModifyRequest(std::string_view request_str) {
    const auto request_type = ConvertModifyRequestTypeFromString(ReadToken(request_str));
    if (!request_type) {
        return nullptr;
    }
    ModifyRequestHolder request = ModifyRequest::Create(*request_type);
    if (request) {
        request->ParseFrom(request_str);
    };
    return request;
}

ModifyRequestHolder ParseModifyRequest(Json::Node& node) {
    const auto request_type = ConvertModifyRequestTypeFromString(node.AsMap().find("type")->second.AsString());
    if (!request_type) {
        return nullptr;
    }
    ModifyRequestHolder request = ModifyRequest::Create(*request_type);
    if (request) {
        request->ParseFrom(node);
    };
    return request;
}


std::vector<ModifyRequestHolder> ReadModifyRequests(std::istream& in_stream = std::cin) {
    const size_t request_count = ReadNumberOnLine<size_t>(in_stream);

    std::vector<ModifyRequestHolder> requests;
    requests.reserve(request_count);

    for (size_t i = 0; i < request_count; ++i) {
        std::string request_str;
        std::getline(in_stream, request_str);
        if (auto request = ParseModifyRequest(request_str)) {
            requests.push_back(std::move(request));
        }
    }
    return requests;
}

std::vector<ModifyRequestHolder> ReadModifyRequests(Json::Document& document) {
    auto requests_array = document.GetRoot().AsMap().find("base_requests")->second.AsArray();
    const size_t request_count = requests_array.size();

    std::vector<ModifyRequestHolder> requests;
    requests.reserve(request_count);

    for (size_t i = 0; i < request_count; ++i) {
        if (auto request = ParseModifyRequest(requests_array[i])) {
            requests.push_back(std::move(request));
        }
    }
    return requests;
}

void ProcessModifyRequests(BusManager& manager, const std::vector<ModifyRequestHolder>& requests) {
    for (const auto& request_holder : requests) {
        const auto& request = static_cast<const ModifyRequest&>(*request_holder);
        request.Process(manager);
    }
    manager.ComputeRouteLenght();
}


#endif

