#include <iostream>
#include "BusRouter.h"
#include "BusManager.h"
#include "RequestManger.h"
#include "test_runner.h"
#include "RefreshManager.h"
#include "json.h"

const std::string test_1(R"(
{"routing_settings": {"bus_wait_time": 6, "bus_velocity" : 40},
"base_requests" :
    [
{"type": "Bus", "name" : "297", "stops" : ["Biryulyovo Zapadnoye", "Biryulyovo Tovarnaya", "Universam", "Biryulyovo Zapadnoye"] , "is_roundtrip" : true},
{ "type": "Bus", "name" : "635", "stops" : ["Biryulyovo Tovarnaya", "Universam", "Prazhskaya"] , "is_roundtrip" : false },
{ "type": "Stop", "name" : "Biryulyovo Zapadnoye", "latitude" : 55.574371, "longitude" : 37.6517, "road_distances" : {"Biryulyovo Tovarnaya": 2600} },
{ "type": "Stop", "name" : "Universam", "latitude" : 55.587655, "longitude" : 37.645687, "road_distances" : {"Biryulyovo Tovarnaya": 1380, "Biryulyovo Zapadnoye" : 2500, "Prazhskaya" : 4650} },
{ "type": "Stop", "name" : "Biryulyovo Tovarnaya", "latitude" : 55.592028, "longitude" : 37.653656, "road_distances" : {"Universam": 890} },
{ "type": "Stop", "name" : "Prazhskaya", "latitude" : 55.611717, "longitude" : 37.603938, "road_distances" : {} }] ,


"stat_requests": [
{"id": 1068356766, "type" : "Bus", "name" : "297"},
{ "id": 1632558877, "type" : "Bus", "name" : "635" },
{ "id": 150108166, "type" : "Stop", "name" : "Universam" },
{ "id": 52006199, "type" : "Route", "from" : "Biryulyovo Zapadnoye", "to" : "Universam" },
{ "id": 1780176932, "type" : "Route", "from" : "Biryulyovo Zapadnoye", "to" : "Prazhskaya" }
]
})");

/*const std::string test_1(R"({   
    "routing_settings": {
    "bus_wait_time": 6,
    "bus_velocity": 40
    }
  "base_requests": [
    {
      "type": "Stop",
      "road_distances": {
        "Marushkino": 3900
      },
      "longitude": 37.20829,
      "name": "Tolstopaltsevo",
      "latitude": 55.611087
    },
    {
      "type": "Stop",
      "road_distances": {
        "Rasskazovka": 9900
      },
      "longitude": 37.209755,
      "name": "Marushkino",
      "latitude": 55.595884
    },
    {
      "type": "Bus",
      "name": "256",
      "stops": [
        "Biryulyovo Zapadnoye",
        "Biryusinka",
        "Universam",
        "Biryulyovo Tovarnaya",
        "Biryulyovo Passazhirskaya",
        "Biryulyovo Zapadnoye"
      ],
      "is_roundtrip": true
    },
    {
      "type": "Bus",
      "name": "750",
      "stops": [
        "Tolstopaltsevo",
        "Marushkino",
        "Rasskazovka"
      ],
      "is_roundtrip": false
    },
    {
      "type": "Stop",
      "road_distances": {},
      "longitude": 37.333324,
      "name": "Rasskazovka",
      "latitude": 55.632761
    },
    {
      "type": "Stop",
      "road_distances": {
        "Rossoshanskaya ulitsa": 7500,
        "Biryusinka": 1800,
        "Universam": 2400
      },
      "longitude": 37.6517,
      "name": "Biryulyovo Zapadnoye",
      "latitude": 55.574371
    },
    {
      "type": "Stop",
      "road_distances": {
        "Universam": 750
      },
      "longitude": 37.64839,
      "name": "Biryusinka",
      "latitude": 55.581065
    },
    {
      "type": "Stop",
      "road_distances": {
        "Rossoshanskaya ulitsa": 5600,
        "Biryulyovo Tovarnaya": 900
      },
      "longitude": 37.645687,
      "name": "Universam",
      "latitude": 55.587655
    },
    {
      "type": "Stop",
      "road_distances": {
        "Biryulyovo Passazhirskaya": 1300
      },
      "longitude": 37.653656,
      "name": "Biryulyovo Tovarnaya",
      "latitude": 55.592028
    },
    {
      "type": "Stop",
      "road_distances": {
        "Biryulyovo Zapadnoye": 1200
      },
      "longitude": 37.659164,
      "name": "Biryulyovo Passazhirskaya",
      "latitude": 55.580999
    },
    {
      "type": "Bus",
      "name": "828",
      "stops": [
        "Biryulyovo Zapadnoye",
        "Universam",
        "Rossoshanskaya ulitsa",
        "Biryulyovo Zapadnoye"
      ],
      "is_roundtrip": true
    },
    {
      "type": "Stop",
      "road_distances": {},
      "longitude": 37.605757,
      "name": "Rossoshanskaya ulitsa",
      "latitude": 55.595579
    },
    {
      "type": "Stop",
      "road_distances": {},
      "longitude": 37.603831,
      "name": "Prazhskaya",
      "latitude": 55.611678
    }
  ],
  "stat_requests": [
    {
      "type": "Bus",
      "name": "256",
      "id": 1965312327
    },
    {
      "type": "Bus",
      "name": "750",
      "id": 519139350
    },
    {
      "type": "Bus",
      "name": "751",
      "id": 194217464
    },
    {
      "type": "Stop",
      "name": "Samara",
      "id": 746888088
    },
    {
      "type": "Stop",
      "name": "Prazhskaya",
      "id": 65100610
    },
    {
      "type": "Stop",
      "name": "Biryulyovo Zapadnoye",
      "id": 1042838872
    }
    {
        "type": "Route",
        "from": "Biryulyovo Zapadnoye",
        "to": "Universam",
        "id": 4
    }
  ]
})");*/

void settings(BusManager& manager, Json::Document& document) {
    auto routing_settings = document.GetRoot().AsMap().find("routing_settings")->second.AsMap();
    size_t waiting_time = ConvertToInt( routing_settings.find("bus_wait_time")->second.AsString());
    double bus_velocity = ConvertToDouble(routing_settings.find("bus_velocity")->second.AsString());
    manager.SetBusVelocity(bus_velocity);
    manager.SetWaitingTime(waiting_time);
}

int main() {
    BusManager manager;
    std::cout.precision(6);
   // std::istringstream in(test_1);
    Json::Document document = Json::Load();
    settings(manager, document);
    const auto refresh = ReadModifyRequests(document);
    ProcessModifyRequests(manager,refresh);
    //manager.BuildRoute("Universam","Biryulyovo Zapadnoye");
    const auto requests = ReadRequests(document);
    const auto responses = ProcessRequests(manager, requests);
    PrintResponses(responses);

    return 0;
}