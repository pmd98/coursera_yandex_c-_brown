#pragma once
#include<string_view>
#include<string>
#include<optional>
#include<exception>
#include <stdexcept>
#include<sstream>

std::string_view Strip(std::string_view s, std::string_view delimeter = " ");


std::pair<std::string_view, std::optional<std::string_view>> SplitTwoStrict(std::string_view s, std::string_view delimiter = " ");

std::pair<std::string_view, std::string_view> SplitTwo(std::string_view s, std::string_view delimiter = " ");
std::string_view ReadToken(std::string_view& s, std::string_view delimiter = " ");


int ConvertToInt(std::string_view str);

double ConvertToDouble(std::string_view str);

bool ConvertToBool(std::string_view str);

template <typename Number>
void ValidateBounds(Number number_to_check, Number min_value, Number max_value);
