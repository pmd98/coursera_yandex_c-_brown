#include "BusManager.h"

using namespace std;


TransportRouter::TransportRouter(const BusManager::StopsDict& stops_dict,
    const BusManager::BusesDict& buses_dict,
    double bus_velocity,
    size_t waiting_time) : graph_(0)
{
    const size_t vertex_count = stops_dict.size() * 2;
    routing_settings_.bus_velocity = bus_velocity;
    routing_settings_.bus_wait_time = waiting_time;
    vertices_info_.resize(vertex_count);
    graph_ = BusGraph(vertex_count);

    FillGraphWithStops(stops_dict);
    FillGraphWithBuses(stops_dict, buses_dict);

    router_ = std::make_unique<Router>(graph_);
}

/*TransportRouter::RoutingSettings TransportRouter::MakeRoutingSettings(const Json::Dict& json) {
    return {
        json.at("bus_wait_time").AsInt(),
        json.at("bus_velocity").AsDouble(),
    };
}*/

void TransportRouter::FillGraphWithStops(const BusManager::StopsDict& stops_dict) {
    Graph::VertexId vertex_id = 0;

    for (const auto& [stop_name, _] : stops_dict) {
        auto& vertex_ids = stops_vertex_ids_[stop_name];
        vertex_ids.in = vertex_id++;
        vertex_ids.out = vertex_id++;
        vertices_info_[vertex_ids.in] = { stop_name };
        vertices_info_[vertex_ids.out] = { stop_name };

        edges_info_.push_back(WaitEdgeInfo{});
        graph_.AddEdge({
            vertex_ids.out,
            vertex_ids.in,
            static_cast<double>(routing_settings_.bus_wait_time)
            });
    }

    assert(vertex_id == graph_.GetVertexCount());
}

void TransportRouter::FillGraphWithBuses(const BusManager::StopsDict& stops_dict,
    const BusManager::BusesDict& buses_dict) {
    for (const auto& [_, bus_item] : buses_dict) {
        const auto& bus = bus_item;
        const size_t stop_count = bus.route.size();
        if (stop_count <= 1) {
            continue;
        }
        auto compute_distance_from = [&stops_dict, &bus](size_t lhs_idx) {
            auto& lhs = bus.route[lhs_idx].stop;
            auto& rhs = bus.route[lhs_idx + 1].stop;
            auto lhs_distances = stops_dict.find(lhs)->second.path;
            if (auto it = lhs_distances.find(rhs); it != lhs_distances.end()) {
                return it->second;
            }
            else {
                auto rhs_distances = stops_dict.find(rhs)->second.path;
                return rhs_distances.at(lhs);
            }

           // return BusManager::ComputeStopsDistance(stops_dict.at(bus.route[lhs_idx].stop), stops_dict.at(bus.route[lhs_idx + 1].stop));
        };
        for (size_t start_stop_idx = 0; start_stop_idx < stop_count; ++start_stop_idx) {
            const Graph::VertexId start_vertex = stops_vertex_ids_[bus.route[start_stop_idx].stop].in;
            int total_distance = 0;
            for (size_t finish_stop_idx = start_stop_idx + 1; finish_stop_idx < stop_count; ++finish_stop_idx) {
                total_distance += compute_distance_from(finish_stop_idx - 1);
                edges_info_.push_back(BusEdgeInfo{
                    .bus_name = _,
                    .span_count = finish_stop_idx - start_stop_idx,
                    });
                graph_.AddEdge({
                    start_vertex,
                    stops_vertex_ids_[bus.route[finish_stop_idx].stop].out,
                    total_distance * 1.0 / (routing_settings_.bus_velocity * 1000.0 / 60)  // m / (km/h * 1000 / 60) = min
                    });
            }
        }
    }
}

optional<TransportRouter::RouteInfo> TransportRouter::FindRoute(const string& stop_from, const string& stop_to) const {
    const Graph::VertexId vertex_from = stops_vertex_ids_.at(stop_from).out;
    const Graph::VertexId vertex_to = stops_vertex_ids_.at(stop_to).out;
    const auto route = router_->BuildRoute(vertex_from, vertex_to);
    if (!route) {
        return nullopt;
    }

    RouteInfo route_info = { .total_time = route->weight };
    route_info.items.reserve(route->edge_count);
    for (size_t edge_idx = 0; edge_idx < route->edge_count; ++edge_idx) {
        const Graph::EdgeId edge_id = router_->GetRouteEdge(route->id, edge_idx);
        const auto& edge = graph_.GetEdge(edge_id);
        const auto& edge_info = edges_info_[edge_id];
        if (holds_alternative<BusEdgeInfo>(edge_info)) {
            const BusEdgeInfo& bus_edge_info = get<BusEdgeInfo>(edge_info);
            route_info.items.push_back(RouteInfo::BusItem{
                .bus_name = bus_edge_info.bus_name,
                .time = edge.weight,
                .span_count = bus_edge_info.span_count,
                });
        }
        else {
            const Graph::VertexId vertex_id = edge.from;
            route_info.items.push_back(RouteInfo::WaitItem{
                .stop_name = vertices_info_[vertex_id].stop_name,
                .time = edge.weight,
                });
        }
    }

    // Releasing in destructor of some proxy object would be better,
    // but we do not expect exceptions in normal workflow
    router_->ReleaseRoute(route->id);
    return route_info;
}

