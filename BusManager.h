#pragma once
#ifndef BusManager_h
#define BusManager_h


#include <algorithm>
#include <cmath>
#include <cstdint>
#include <ctime>
#include <exception>
#include <iostream>
#include <iterator>
#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <string>
#include <system_error>
#include <type_traits>
#include <unordered_set>
#include <unordered_map>
#include <variant>
#include <vector>


#include "graph.h"
#include "json.h"
#include "route.h"

//#include "BusRouter.h"
//using namespace std;

struct Stop {
    std::unordered_map<std::string, size_t> path;
    std::set<std::string> buses_on_stop;
    double latitude, longitude;
    size_t id;
};

struct passage
{
    std::string stop;
    size_t path_to_next;
    //double perfect_path_to_next;
    double travel_time;
};

struct Route {
    double curvature;
    size_t path;
    double perfect_path;
    size_t stop_count;
    bool cycle;
    std::unordered_set<std::string> unique_stops;
    std::vector<passage> route;
};


using StopsDict = std::unordered_map<std::string, Stop>;
using BusesDict = std::unordered_map<std::string, Route>;


class TransportRouter {
private:
    using BusGraph = Graph::DirectedWeightedGraph<double>;
    using Router = Graph::Router<double>;

public:

    TransportRouter(const StopsDict& stops_dict,
        const BusesDict& buses_dict,
        double bus_velocity,
        size_t waiting_time);

    struct RouteInfo {
        double total_time;

        struct BusItem {
            std::string bus_name;
            double time;
            size_t span_count;
        };
        struct WaitItem {
            std::string stop_name;
            double time;
        };

        using Item = std::variant<BusItem, WaitItem>;
        std::vector<Item> items;
    };

    std::optional<RouteInfo> FindRoute(const std::string& stop_from, const std::string& stop_to) const;

private:
    struct RoutingSettings {
        int bus_wait_time;  // in minutes
        double bus_velocity;  // km/h
    };

    //static RoutingSettings MakeRoutingSettings(const Json::Dict& json);


    void FillGraphWithStops(const StopsDict& stops_dict);

    void FillGraphWithBuses(const StopsDict& stops_dict,
        const BusesDict& buses_dict);

    struct StopVertexIds {
        Graph::VertexId in;
        Graph::VertexId out;
    };
    struct VertexInfo {
        std::string stop_name;
    };

    struct BusEdgeInfo {
        std::string bus_name;
        size_t span_count;
    };
    struct WaitEdgeInfo {};
    using EdgeInfo = std::variant<BusEdgeInfo, WaitEdgeInfo>;

    RoutingSettings routing_settings_;
    BusGraph graph_;
    std::unique_ptr<Router> router_;
    std::unordered_map<std::string, StopVertexIds> stops_vertex_ids_;
    std::vector<VertexInfo> vertices_info_;
    std::vector<EdgeInfo> edges_info_;
};

class BusManager {
public:

    size_t ComputeStopsDistance(const std::string& lhs, const std::string& rhs) {
        auto lhs_distances = stops.find(lhs)->second.path;
        if (auto it = lhs_distances.find(rhs); it != lhs_distances.end()) {
            return it->second;
        }
        else {
            auto rhs_distances = stops.find(rhs)->second.path;
            return rhs_distances.at(lhs);
        }
    }


   

    using StopsDict = std::unordered_map<std::string, Stop>;
    using BusesDict = std::unordered_map<std::string, Route>;

private:
    size_t stop_count = 0;
    size_t bus_waiting_time = 0;
    double bus_velocity = 0.0;

   
    std::unique_ptr<TransportRouter> router;
    
    
    StopsDict stops;
    BusesDict buses;



    void ComputeRouteLenght(Route& route) {
        
        for (size_t i = 1; i < route.route.size(); ++i) {
            size_t buf = ComputeRealPath(route.route[i - 1].stop, route.route[i].stop);
            route.path +=buf;
            route.route[i - 1].path_to_next = buf;
            route.route[i - 1].travel_time = (buf * 1.0) / bus_velocity;
            route.perfect_path += ComputePath(route.route[i - 1].stop, route.route[i].stop);

        }
       /* if (!route.cycle) {
            route.perfect_path *= 2;
            for (size_t i = route.route.size() - 1; i > 0; --i) {
                route.path += ComputeRealPath(route.route[i].stop, route.route[i-1].stop);
            }
        }*/
    }
    

    size_t ComputeRealPath(const std::string& lhs_stop, const std::string& rhs_stop) const {
        auto lhs_stop_iter = stops.at(lhs_stop);
        auto lhs_iter = lhs_stop_iter.path.find(rhs_stop);
        if (lhs_iter == lhs_stop_iter.path.end()) {
            return stops.at(rhs_stop).path.find(lhs_stop)->second;
        }
        return lhs_iter->second;
    }


    double ComputePath(const std::string& lhs_stop, const std::string& rhs_stop) const {
        constexpr double PI = 3.1415926535;
        constexpr double RADIUS = 6'371'000;
        double lhs_longtitude = stops.at(lhs_stop).longitude * PI / 180.0;
        double lhs_latitude = stops.at(lhs_stop).latitude * PI / 180.0;

        double rhs_longtitude = stops.at(rhs_stop).longitude * PI / 180.0;
        double rhs_latitude = stops.at(rhs_stop).latitude * PI / 180.0;

        return RADIUS * std::asin(
            std::sqrt(
                std::pow(std::sin(rhs_latitude - lhs_latitude), 2.0) +
                std::cos(lhs_latitude) *
                std::cos(rhs_latitude) *
                std::pow(std::sin(rhs_longtitude - lhs_longtitude), 2.0)
            )
        );
    }

   
    
public:

    void BuildRoute() {
        router = std::make_unique<TransportRouter>(stops, buses, bus_velocity, bus_waiting_time);
    }
 //   explicit BusManager() : graph(std::make_unique<Graph::DirectedWeightedGraph<double>>(NULL)),
   //     router(std::make_unique<Graph::Router<double>>(*graph.get())) {}

    void SetWaitingTime(size_t waiting_time) { bus_waiting_time = waiting_time; }
    void SetBusVelocity(double bus_velocity) { this->bus_velocity = bus_velocity; }

    void AddStop(const std::string& name, double latitude, double longitude,
        const std::unordered_map<std::string,size_t> path) {
        auto& stop = stops[name];
        stop.latitude = latitude;
        stop.longitude = longitude;
        stop.path = path;
        stop.id = stop_count;
        ++stop_count;
    }

    void AddBus(const std::string& bus_name,const std::vector<std::string>& route, bool cycle) {
        auto& bus = buses[bus_name];
        bus.cycle = cycle;
        bus.stop_count = route.size();
        if (!bus.cycle) {
            bus.stop_count *= 2;
            --bus.stop_count;
        }
       // all_bus_stop += bus.stop_count;
        bus.route.reserve(bus.stop_count);
        if (bus.cycle) {
            for (const auto& stop : route) {
                stops[stop].buses_on_stop.insert(bus_name);
                bus.route.push_back({ .stop = stop, .path_to_next = 0, .travel_time = 0.0 });
                bus.unique_stops.insert(stop);
            }
        }
        else {
            for (size_t i = 0; i < bus.stop_count; ++i) {
                size_t index = i;
                if (index >= route.size()) {
                    index -= (i - route.size() + 1) * 2;
                }
                stops[route[index]].buses_on_stop.insert(bus_name);
                bus.unique_stops.insert(route[index]);
                bus.route.push_back({ .stop = route[index], .path_to_next = 0, .travel_time = 0.0 });
            }
        }
    }

    void ComputeRouteLenght() {
        for (auto& bus : buses) {
            ComputeRouteLenght(bus.second);
            bus.second.curvature = (bus.second.path * 1.0) / bus.second.perfect_path;
        }
    }

    bool BusContain(const std::string& bus_name) const {
        if (auto iter = buses.find(bus_name); iter != buses.end())
            return true;
        return false;
    }

    bool StopContain(const std::string& stop_name) const {
        if (auto iter = stops.find(stop_name); iter != stops.end())
            return true;
        return false;
    }

    

    auto FindRoute(const std::string& from, const std::string& to) const {
        return router->FindRoute(from, to);
    }

    
    /*auto BuildRoute(const std::string& from, const std::string& to) const {
        return router->BuildRoute(stops.at(from).id,stops.at(to).id);
    }
     
    auto GetRouteEdge(uint64_t route_id, size_t vertex_id) const {
        return router->GetRouteEdge(route_id,vertex_id); 
    }*/

    /*void BuildGraph() {
        graph = std::move(std::make_unique<Graph::DirectedWeightedGraph<double>>(stop_count));
        for (const auto& [stop_name, stop_info] : stops) {
            for (size_t i = 1; i < stop_info.path.size(); ++i) {
                Graph::Edge<double> edge;
                
                //edge.from = stops.at(bus.route[i - 1].stop).id;
                //edge.to = stops.at(bus.route[i].stop).id;
                //edge.weight = bus.route[i - 1].travel_time;
                graph->AddEdge(edge);
            }
        }
        router = std::make_unique<Graph::Router<double>>((*graph.get()));
    }*/


    /*void BuildGraph() {
        graph = std::move(std::make_unique<Graph::DirectedWeightedGraph<double>>(stop_count));
        for (const auto& [bus_name, bus] : buses) {
            for (size_t i = 1; i < bus.route.size(); ++i) {
                Graph::Edge<double> edge;
                edge.from = stops.at(bus.route[i - 1].stop).id;
                edge.to = stops.at(bus.route[i].stop).id;
                edge.weight = bus.route[i-1].travel_time;
                graph->AddEdge(edge);
            }
        }
        router = std::make_unique<Graph::Router<double>>((*graph.get()));
    }*/

    const Route& GetRoute(const std::string& bus_name) const {
        return  buses.at(bus_name);
    }

    const auto& GetBusesOnStop(const std::string& stop_name) const{
        return stops.at(stop_name).buses_on_stop;
    }

};



#endif
