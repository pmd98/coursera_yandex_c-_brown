#include "test_runner.h"

#include <iostream>
#include <map>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <list>

using namespace std;

struct Record {
    string id;
    string title;
    string user;
    int timestamp;
    int karma;

    bool operator==(const Record& other) const{
        return (id == other.id) &&
            (title == other.title) &&
            (user == other.user) &&
            (timestamp == other.timestamp) &&
            (karma == other.karma);
    }
};

struct RecordHasher
{
    size_t operator()(const Record& record) const{
        const size_t x = 23;
        return x * x * x * x * hash_string(record.id) +
            x * x * x * hash_string(record.title) +
            x * x * hash_string(record.user) + 
            x * hash_int(record.timestamp) +
            hash_int(record.karma);
    }

    hash<int> hash_int;
    hash<string> hash_string;
};

// ���������� ���� �����
class Database {
public:
    bool Put(const Record& record);

    const Record* GetById(const string& id) const;

    bool Erase(const string& id);

    template <typename Callback>
    void RangeByTimestamp(int low, int high, Callback callback) const;

    template <typename Callback>
    void RangeByKarma(int low, int high, Callback callback) const;

    template <typename Callback>
    void AllByUser(const string& user, Callback callback) const;

private:
    using pointer = typename list<Record>::iterator;
    struct pointer_list{
        pointer ptr_to_data;
        multimap<string, pointer>::iterator ptr_to_user_map;
        multimap<int, pointer>::iterator ptr_to_karma_map;
        multimap<int, pointer>::iterator ptr_to_timestamp_map;
    };
    list<Record> data;
    unordered_map<string, pointer_list> search_by_id;
    multimap<string, pointer> search_by_user;
    multimap<int, pointer> search_by_karma;
    multimap<int, pointer> search_by_timestamp;
};

bool Database::Put(const Record& record)
{
    auto iter = search_by_id.find(record.id);
    if (iter == search_by_id.end()) {
        pointer insert_iter = data.insert(data.end(),record);
        auto iter_user = search_by_user.insert({ record.user,insert_iter });
        auto iter_karma = search_by_karma.insert({ record.karma,insert_iter });
        auto iter_time = search_by_timestamp.insert({ record.timestamp,insert_iter });
        search_by_id[record.id] = { insert_iter,iter_user,iter_karma,iter_time };
        return true;
    }
    return false;
}

const Record* Database::GetById(const string& id) const
{
    auto iter = search_by_id.find(id);
    if (iter != search_by_id.end()) {
        pointer result = iter->second.ptr_to_data;
        return &(*result) ;
    }
    return nullptr;
}

bool Database::Erase(const string& id)
{
    auto iter = search_by_id.find(id);
    if (iter != search_by_id.end()) {
        data.erase(iter->second.ptr_to_data);
        search_by_user.erase(iter->second.ptr_to_user_map);
        search_by_karma.erase(iter->second.ptr_to_karma_map);
        search_by_timestamp.erase(iter->second.ptr_to_timestamp_map);
        search_by_id.erase(iter);
        return true;
    }
    return false;
}

template<typename Callback>
void Database::RangeByTimestamp(int low, int high, Callback callback) const
{
    auto lower = search_by_timestamp.lower_bound(low);
    const auto upper = search_by_timestamp.upper_bound(high);

    while (lower != upper && callback(*lower->second))
    {
        ++lower;
    }
/*    for (auto iter = search_by_timestamp.find(low); 
        iter != search_by_timestamp.end() &&
        iter->second->timestamp <= high;
        ++iter) {
        if (!callback(*(iter->second)))
            break;
    }*/
}

template<typename Callback>
void Database::RangeByKarma(int low, int high, Callback callback) const
{
    auto lower = search_by_karma.lower_bound(low);
    const auto upper = search_by_karma.upper_bound(high);

    while (lower != upper && callback(*lower->second))
    {
        ++lower;
    }
    /*for (auto iter = search_by_karma.find(low);
        iter != search_by_karma.end() &&
        iter->second->karma <= high;
        ++iter) {
        if (!callback(*(iter->second)))
            break;
    }*/
}

template<typename Callback>
void Database::AllByUser(const string& user, Callback callback) const
{
    auto [first, end] = search_by_user.equal_range(user);

    while (first != end && callback(*first->second))
    {
        ++first;
    }
    /*for (auto iter = search_by_user.find(user);
        iter != search_by_user.end() &&
        iter->second->user == user;
        ++iter) {
        if (!callback(*(iter->second)))
            break;
    }*/
}



void TestRangeBoundaries() {
    const int good_karma = 1000;
    const int bad_karma = -10;

    Database db;
    db.Put({ "id1", "Hello there", "master", 1536107260, good_karma });
    db.Put({ "id2", "O>>-<", "general2", 1536107260, bad_karma });
  

    int count = 0;
    db.RangeByKarma(bad_karma, good_karma, [&count](const Record&) {
        ++count;
        return true;
        });

    ASSERT_EQUAL(2, count);

    count = 0;
    const int min_time = 1536107260;
    const int max_time = 1536107261;
    db.Put({ "id3", "O>>-<", "general21", 1536107261, bad_karma });
    db.RangeByTimestamp(min_time, max_time, [&count](const Record&) {
        ++count;
        return true;
        });
}

void TestSameUser() {
    Database db;
    db.Put({ "id1", "Don't sell", "master", 1536107260, 1000 });
    db.Put({ "id2", "Rethink life", "master", 1536107260, 2000 });

    int count = 0;
    db.AllByUser("master", [&count](const Record&) {
        ++count;
        return true;
        });

    ASSERT_EQUAL(2, count);
}

void TestReplacement() {
    const string final_body = "Feeling sad";

    Database db;
    db.Put({ "id", "Have a hand", "not-master", 1536107260, 10 });
    db.Erase("id");
    db.Put({ "id", final_body, "not-master", 1536107260, -10 });

    auto record = db.GetById("id");
    ASSERT(record != nullptr);
    ASSERT_EQUAL(final_body, record->title);
}

int main() {
    TestRunner tr;
    RUN_TEST(tr, TestRangeBoundaries);
    RUN_TEST(tr, TestSameUser);
    RUN_TEST(tr, TestReplacement);
    return 0;
}

