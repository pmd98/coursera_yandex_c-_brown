#include "test_runner.h"
#include <functional>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

using namespace std;


struct Email {
    string from;
    string to;
    string body;
};


inline std::istream& operator>>(std::istream& input, Email& email)
{
//    std::string from, to, body;
    std::getline(input, email.from);
    std::getline(input, email.to);
    std::getline(input, email.body);
    //email = Email{ std::move(from), std::move(to), std::move(body) };
    return input;
}


inline std::ostream& operator<<(std::ostream& output, const Email& email)
{
    output << email.from << '\n';
    output << email.to << '\n';
    output << email.body << '\n';

    return output;
}


class Worker {
public:
    virtual ~Worker() = default;
    virtual void Process(unique_ptr<Email> email) = 0;
    virtual void Run() {
        // ������ ������� worker-� � ��������� ����� ��� ����������������
        throw logic_error("Unimplemented");
    }

protected:
    // ���������� ������ �������� PassOn, ����� �������� ������ ������
    // �� ������� ������������
    void PassOn(unique_ptr<Email> email) const {
        if(next_)
        next_.get()->Process(move(email));
    }

public:
    void SetNext(unique_ptr<Worker> next) {
        if (!next_) {
            next_ = move(next);
        }
        else {
            next_.get()->SetNext(move(next));
        }
    }

private:
    unique_ptr<Worker> next_;
};


class Reader : public Worker {
public:
    // ���������� �����
    explicit Reader(istream& reader) : reader_(reader) {}

    virtual void Process(unique_ptr<Email> email) override {
        PassOn(move(email));
    }

    void Run() override {
        Email email;
        while (reader_ >> email)
        {
            auto result = make_unique<Email>(move(email));
            this->Process(move(result));
        }
    }
private:
    istream& reader_;
};


class Filter : public Worker {
public:
    using Function = function<bool(const Email&)>;
    
    explicit Filter(Function pred) : pred_(pred) {}
    
    void Process(unique_ptr<Email> email) override {
        if (pred_(*email.get())) {
            PassOn(move(email)); // �������� ������
        }
    }

public:
    Function pred_;
    // ���������� �����
};


class Copier : public Worker {
public:
    explicit Copier(const string& to ) : to_ (to){}

    void Process(unique_ptr<Email> email) override {
        if (to_ == email.get()->to) {
            PassOn(move(email)); // �������� ������
        }
        else
        {
            Email copy_email {email.get()->from, to_,email.get()->body };
            auto copy_email_ptr = make_unique<Email>(copy_email);
            PassOn(move(email));
            PassOn(move(copy_email_ptr)); // �������� ������
        }
    }
    // ���������� �����
private:
    const string to_;
};


class Sender : public Worker {
public:
    explicit Sender(ostream& out) : out_(out){}

    void Process(unique_ptr<Email> email) override {
        out_ << email.get()->from  << "\n" 
            << email.get()->to << "\n"
            << email.get()->body << "\n";
        PassOn(move(email));
    }
private:
    ostream& out_;
};


// ���������� �����
class PipelineBuilder {
public:
    // ��������� � �������� ������� ����������� Reader
    explicit PipelineBuilder(istream& in) {
        Head = make_unique<Reader>(in);
    }

    // ��������� ����� ���������� Filter
    PipelineBuilder& FilterBy(Filter::Function filter) {
        unique_ptr<Worker> filter_ = make_unique<Filter>(filter);
        Head.get()->SetNext(move(filter_));
        return *this;
    }

    // ��������� ����� ���������� Copier
    PipelineBuilder& CopyTo(string recipient) {
        unique_ptr<Worker> copier = make_unique<Copier>(recipient);
        Head.get()->SetNext(move(copier));
        return *this;
    }

    // ��������� ����� ���������� Sender
    PipelineBuilder& Send(ostream& out) {
        unique_ptr<Worker> sender = make_unique<Sender>(out);
        Head.get()->SetNext(move(sender));
        return *this;
    }

    // ���������� ������� ������� ������������
    unique_ptr<Worker> Build() {
        return move(Head);
    }

private:
    unique_ptr<Worker> Head;
};


void TestSanity() {
    string input = (
        "erich@example.com\n"
        "richard@example.com\n"
        "Hello there\n"

        "erich@example.com\n"
        "ralph@example.com\n"
        "Are you sure you pressed the right button?\n"

        "ralph@example.com\n"
        "erich@example.com\n"
        "I do not make mistakes of that kind\n"
        );
    istringstream inStream(input);
    ostringstream outStream;

    PipelineBuilder builder(inStream);
    builder.FilterBy([](const Email& email) {
        return email.from == "erich@example.com";
        });
    builder.CopyTo("richard@example.com");
    builder.Send(outStream);
    auto pipeline = builder.Build();

    pipeline->Run();

    string expectedOutput = (
        "erich@example.com\n"
        "richard@example.com\n"
        "Hello there\n"

        "erich@example.com\n"
        "ralph@example.com\n"
        "Are you sure you pressed the right button?\n"

        "erich@example.com\n"
        "richard@example.com\n"
        "Are you sure you pressed the right button?\n"
        );

    ASSERT_EQUAL(expectedOutput, outStream.str());
}

int main() {
    TestRunner tr;
    RUN_TEST(tr, TestSanity);
    return 0;
}