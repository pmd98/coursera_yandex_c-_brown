#include "test_runner.h"

#include <functional>
#include <string>
#include <utility>
#include <optional>
using namespace std;

template <typename T>
class LazyValue {
public:
    explicit LazyValue(std::function<T()> init) : value(nullopt), initializer(init), flag(false){}

    bool HasValue() const {
        return flag;
    }

    const T& Get() const {
        if (flag)
            return value.value();
        flag = true;
        //value = new T;
        value = std::optional<T>(initializer());
        return value.value();
    }

   /* ~LazyValue() {
        if (HasValue())
            delete value;
        value = nullptr;
    }*/

private:
    //mutable T* value;
    std::function<T()> initializer;
    mutable std::optional<T> value;
    mutable bool flag;
};

void UseExample() {
    const string big_string = "Giant amounts of memory";

    LazyValue<string> lazy_string([&big_string] { return big_string; });

    ASSERT(!lazy_string.HasValue());
    ASSERT_EQUAL(lazy_string.Get(), big_string);
    ASSERT_EQUAL(lazy_string.Get(), big_string);
}

void TestInitializerIsntCalled() {
    bool called = false;

    {
        LazyValue<int> lazy_int([&called] {
            called = true;
            return 0;
            });
    }
    ASSERT(!called);
}

void DoSomething(const vector<string>& v) {
    for (auto it = v.begin(); it != v.end(); ++it) {
        auto s = *it;
    }
}

int main() {
    TestRunner tr;
    RUN_TEST(tr, UseExample);
    RUN_TEST(tr, TestInitializerIsntCalled);
    return 0;
}