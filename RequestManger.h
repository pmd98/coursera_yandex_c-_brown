#pragma once
#ifndef RequestManager_h
#define RequestManager_h

#include <cstdint>
#include <exception>
#include <iostream>
#include <iterator>
#include <memory>
#include <optional>
#include <sstream>
#include <string>
#include <system_error>
#include <type_traits>
#include <unordered_map>
#include <vector>
#include <variant>
#include <iomanip>

#include "BusManager.h"
#include "parse.h"
#include "json.h"
#include "route.h"

struct Request;
using RequestHolder = std::unique_ptr<Request>;



struct Request {
    enum class Type {
        BUS,
        STOP,
        ROUTE
    };
    Request(Type type) : type(type) {}
    static RequestHolder Create(Type type);
    //virtual void ParseFrom(std::string_view input) = 0;
    virtual void ParseFrom(Json::Node& node) = 0;
    virtual ~Request() = default;

    const Type type;
    size_t stat_request_id = 0;
};

const std::unordered_map<std::string_view, Request::Type> STR_TO_REQUEST_TYPE = {
    {"Bus", Request::Type::BUS},
    {"Stop", Request::Type::STOP},
    {"Route", Request::Type::ROUTE}
};

template <typename ResultType>
struct ReadRequest : Request {
    
    using Request::Request;
    virtual ResultType Process(const BusManager& manager) const = 0;
};


struct BusRequest : ReadRequest<std::string> {
    BusRequest() : ReadRequest(Type::BUS) {}

    std::string Process(const BusManager& manager) const override {
        std::ostringstream result;
        result << "\"request_id\" : " << stat_request_id << ",";
        if (manager.BusContain(bus_name)) {
            const auto& route = manager.GetRoute(bus_name);
            result <<
                "\"stop_count\" : " << route.stop_count << ", " <<
                "\"unique_stop_count\" : " << route.unique_stops.size() << ", " <<
                "\"route_length\" : " << route.path << ", " <<
                "\"curvature\" : " << route.curvature << " ";
        }
        else {
            result << "\"error_message\" : \"not found\" ";
        }
        return result.str();
    }

    void ParseFrom(Json::Node& node) override {
        bus_name = node.AsMap().find("name")->second.AsString();
        stat_request_id = ConvertToInt( node.AsMap().find("id")->second.AsString());
    }

    std::string bus_name;
};

struct StopRequest : ReadRequest<std::string> {
    StopRequest() : ReadRequest(Type::STOP) {}

    void ParseFrom(Json::Node& node) override {
        stop_name = node.AsMap().find("name")->second.AsString();
        stat_request_id = ConvertToInt(node.AsMap().find("id")->second.AsString());
    }

    std::string Process(const BusManager& manager) const override {
        std::ostringstream result;
        result << "\"request_id\" : " << stat_request_id << ",";
        if (manager.StopContain(stop_name)) {
            const auto& buses_on_stop = manager.GetBusesOnStop(stop_name);
            if (!buses_on_stop.empty()) {
                result << "\"buses\" : [ ";
                size_t i = 0;
                for (const std::string& bus : buses_on_stop) {
                    result << "\"" << bus << "\" ";
                    if (i + 1 < buses_on_stop.size())
                        result << ",";
                    /*else
                        result << "\n";*/
                    ++i;
                }
                result << "]";
            }
            else {
                result << "\"buses\" : [] ";
            }
        }
        else {
            result << "\"error_message\" : \"not found\" ";
        }
        return result.str();
    }

    std::string stop_name;
};

struct RouteItemResponseBuilder {
    std::string operator()(const TransportRouter::RouteInfo::BusItem& bus_item) const {
        std::ostringstream result;
        result
            << "\"type\" : \"Bus\", "
            << "\"bus\" : \"" << bus_item.bus_name << "\", "
            << "\"span_count\" : " << bus_item.span_count << ", "
            << "\"time\" : " << bus_item.time;
        return result.str();
    }
    std::string operator()(const TransportRouter::RouteInfo::WaitItem& wait_item) const {
        std::ostringstream result;
        result 
            << "\"type\" : \"Wait\", "
            << "\"stop_name\" : \"" << wait_item.stop_name << "\", "
            << "\"time\" : " << wait_item.time;
        return result.str();
    }
};
struct RouteRequest : ReadRequest<std::string> {

    RouteRequest() : ReadRequest(Type::ROUTE) {}
    
    std::string Process(const BusManager& manager) const override {
        std::ostringstream result;
        result << "\"request_id\" : " << stat_request_id << ",";
        auto route_info = manager.FindRoute(from, to);
        //auto route_info = manager.BuildRoute(from,to);
        if (route_info.has_value()) {
            result <<
                "\"total_time\" : " << route_info->total_time << ", " <<
                "\"items\" : [";
            size_t i = 0;
            for (const auto& item : route_info->items) {
                result << "{";
                result << std::visit(RouteItemResponseBuilder{}, item);
                result << "}";
                if (i + 1 < route_info->items.size()) {
                    result << ", ";
                }
                ++i;
            }
            result << "]";
        }
        else {
            result << "\"error_message\" : \"not found\" ";
        }
        return result.str();
    }


    void ParseFrom(Json::Node& node) override {
        from = node.AsMap().find("from")->second.AsString();
        to = node.AsMap().find("to")->second.AsString();
        stat_request_id = ConvertToInt(node.AsMap().find("id")->second.AsString());
    }

    std::string from, to;
};

RequestHolder Request::Create(Request::Type type) {
    switch (type) {
    case Request::Type::BUS:
        return std::make_unique<BusRequest>();
    case Request::Type::STOP:
        return std::make_unique<StopRequest>();
    case Request::Type::ROUTE:
        return std::make_unique<RouteRequest>();
        return nullptr;
    }
}



template <typename Number>
Number ReadNumberOnLine(std::istream& stream) {
    Number number;
    stream >> number;
    std::string dummy;
    std::getline(stream, dummy);
    return number;
}

std::optional<Request::Type> ConvertRequestTypeFromString(std::string_view type_str) {
    if (const auto it = STR_TO_REQUEST_TYPE.find(type_str);
        it != STR_TO_REQUEST_TYPE.end()) {
        return it->second;
    }
    else {
        return std::nullopt;
    }
}

RequestHolder ParseRequest(Json::Node& node) {
    const auto request_type = ConvertRequestTypeFromString(node.AsMap().find("type")->second.AsString());
    if (!request_type) {
        return nullptr;
    }
    RequestHolder request = Request::Create(*request_type);
    if (request) {
        request->ParseFrom(node);
    };
    return request;
}


std::vector<RequestHolder> ReadRequests(Json::Document& document) {
    auto requests_array = document.GetRoot().AsMap().find("stat_requests")->second.AsArray();
    const size_t request_count = requests_array.size();

    std::vector<RequestHolder> requests;
    requests.reserve(request_count);

    for (size_t i = 0; i < request_count; ++i) {
        if (auto request = ParseRequest(requests_array[i])) {
            requests.push_back(std::move(request));
        }
    }
    return requests;
}

std::vector<std::string> ProcessRequests(const BusManager& manager, const std::vector<RequestHolder>& requests) {

    std::vector<std::string> responses;
    for (const auto& request_holder : requests) {
        const auto& request = static_cast<const BusRequest&>(*request_holder);
        responses.push_back(request.Process(manager));
    }
    return responses;
}

void PrintResponses(const std::vector<std::string>& responses, std::ostream& stream = std::cout) {
   /* for (const std::string& response : responses) {
        stream << response << "\n";
    }*/
    //stream << "[\n";
    stream << "[";
    size_t i = 0;
    for (const std::string& response : responses) {
        //stream << "{\n";
        stream << "{";
        stream << response;
        
        if(i + 1 <responses.size() )
            stream << "},";
        else
            stream << "}";
        ++i;

    }
    stream << "]";
}

#endif