#include "Common.h"
#include <stdexcept> 
using namespace std;

// ���� ���� ������ �� ��������
// ����� �������� ���������� ����������� �������-�������� `IShape`
class Figure : public IShape {
public:
    // ���������� ������ ����� ������.
// ���� ������ �������� ��������, �� ��������� ����� �������� �� �� �����
// ��������. ������ � � ����� ��������� ������� ���� ���������.
    std::unique_ptr<IShape> Clone() const override {
        Figure clone;
        clone.position = position;
        clone.size = size;
        clone.texture = texture;
        return std::make_unique<Figure>(std::move(clone));
    }

    void SetPosition(Point position) override { this->position = position; }
    Point GetPosition() const override { return position; }

    void SetSize(Size size) override { this->size = size; }
    Size GetSize() const override { return size; }

    void SetTexture(std::shared_ptr<ITexture> texture) override {this->texture = texture;}
    ITexture* GetTexture() const override { return texture.get(); }

    // ������ ������ �� ��������� �����������
    void Draw(Image& image) const override {
        /*texture.get()->GetSize().height;
        texture.get()->GetSize().width;
        size.height;
        size.width;
        image[position.y][position.x];*/
        for (size_t i = 0; i < size.height && position.y + i < image.size();++i) {
            for (size_t j = 0; j < size.width && position.x + j < image[i].size(); ++j) {
                if (texture &&
                    i < texture.get()->GetSize().height&&
                    j < texture.get()->GetSize().width)
                {
                    image[position.y + i][position.x + j] = texture.get()->GetImage()[i][j];
                }
                else {
                    image[position.y + i][position.x + j] = '.';
                }
            }
        }
    }

protected:
    Point position;
    Size size;
    shared_ptr<ITexture> texture;
};

class Rectangle : public Figure {
//public:
    // ������ ������ �� ��������� �����������
   // void Draw(Image&) const override {}
};


class Ellipse : public Figure {
    // ������ ������ �� ��������� �����������
    void Draw(Image& image) const override {
        for (size_t i = 0; i < size.height && position.y + i < image.size(); ++i) {
            for (size_t j = 0; j < size.width && position.x + j < image[i].size() ; ++j) {
               // int a = texture.get()->GetSize().width;
               // int b = texture.get()->GetSize().height;
                if (!IsPointInEllipse({int(j),int(i)}, size))
                    continue;
                if (texture &&
                    i < texture.get()->GetSize().height &&
                    j < texture.get()->GetSize().width)
                {
                    image[position.y + i][position.x + j] = texture.get()->GetImage()[i][j];
                }
                else {
                    image[position.y + i][position.x + j] = '.';
                }
            }
        }
    }
};
// �������� ���������� �������

unique_ptr<IShape> MakeShape(ShapeType shape_type) {
    switch (shape_type)
    {
    case ShapeType::Ellipse: {
        return make_unique<Ellipse>();
        break; 
    }
    case ShapeType::Rectangle: {
        return make_unique<Rectangle>();
        break; 
    }
    default: {
        throw invalid_argument("Unkmown figure type!");
        break; 
    }
    }
}