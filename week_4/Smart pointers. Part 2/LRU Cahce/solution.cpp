#include "Common.h"
//#include "synhronized.h"
#include <unordered_map>
#include <list>
#include<mutex>
using namespace std;

template <typename T>
class Synchronized {
public:
    explicit Synchronized(T initial = T()) : value_(std::move(initial)) {}

    struct Access
    {
        Access(T& value, std::mutex& mutex) : ref_to_value(value), guard_(mutex) {}

        T& ref_to_value;
    private:
        std::lock_guard<std::mutex> guard_;
    };

    Access GetAccess();
private:
    T value_;
    std::mutex mutex_;
};


template <typename T>
typename Synchronized<T>::Access Synchronized<T>::GetAccess()
{
    return { value_, mutex_ };
}



class LruCache : public ICache {
public:
    LruCache(
        shared_ptr<IBooksUnpacker> books_unpacker,
        const Settings& settings
    ) : max_size (settings.max_memory), size(NULL), books_unpacker(move(books_unpacker)) {}

    BookPtr GetBook(const string& book_name) override {
        BookPtr result;
       // auto& result = result.GetAccess().ref_to_value;
       // auto& cahce_access = cahce.GetAccess().ref_to_value;
       // auto& priority_cahce_access = priority_cahce.GetAccess().ref_to_value;
       
        lock_guard<mutex> guard(_mutex);
        auto book_iter = cahce.find(book_name);
        if (book_iter == cahce.end()) { // not found in cache
            unique_ptr<IBook> new_book = books_unpacker->UnpackBook(book_name);
            size_t new_book_size = new_book->GetContent().size();
            if (new_book_size > max_size) {
                cahce.clear();
                priority_cahce.clear();
                size = 0;
            }
            else {
                while (new_book_size + size > max_size) { // free memory for new book
                    BookPtr book_ptr = move(priority_cahce.back());
                    priority_cahce.pop_back();
                    cahce.erase(book_ptr->GetName());
                    size -= book_ptr->GetContent().size();
                }
                result = BookPtr(new_book.release());
                //result.reset(new_book.release());
                size += new_book_size;
                priority_cahce.push_front(result);
                cahce[book_name] = priority_cahce.begin();
            }
        }
        else { 
            result = move(*(book_iter->second));
            priority_cahce.erase(book_iter->second);
            priority_cahce.push_front(result);
            cahce[result->GetName()] = priority_cahce.begin();
        }
        return result;
    }

private:
    shared_ptr<IBooksUnpacker> books_unpacker;
    unordered_map<string, typename list<BookPtr>::iterator> cahce;
    list<BookPtr> priority_cahce;
    size_t max_size;
    size_t size;
    mutex _mutex;
    
};

unique_ptr<ICache> MakeCache(
    shared_ptr<IBooksUnpacker> books_unpacker,
    const ICache::Settings& settings
) {
    return make_unique<LruCache>(books_unpacker,settings);
}