#include "test_runner.h"

#include <cstddef>  // ����� ��� nullptr_t

using namespace std;

// ���������� ������ ������ UniquePtr
template <typename T>
class UniquePtr {
private:
    T* pointer;
    //bool status;
public:
    UniquePtr() : pointer(nullptr) {}

    UniquePtr(T* ptr) : pointer(ptr) {
        ptr = nullptr;
    }

    UniquePtr(const UniquePtr&) = delete;
    UniquePtr(UniquePtr&& other) : pointer(std::move(other.pointer)) {
        other.pointer = nullptr;
    }

    UniquePtr& operator = (const UniquePtr&) = delete;
    UniquePtr& operator = (nullptr_t) {
        if (pointer) {
            delete pointer;
        }
        pointer = nullptr;
        return (*this);
    }
    
    UniquePtr& operator = (UniquePtr&& other) {
        if (pointer != other.pointer) {
            if (pointer) {
                delete pointer;
            }
            pointer = std::move(other.pointer);
            other.pointer = nullptr;
        }
        return (*this);
    }

    ~UniquePtr() { 
        if (pointer) {
            delete pointer;
            pointer = nullptr;
        }
    }

    T& operator * () const {
        return *pointer;
    }

    T* operator -> () const {
        return pointer;
    }

    T* Release() {
        T* result = pointer;
        pointer = nullptr;
        return result;
    }

    void Reset(T* ptr) {
        if (pointer) {
            delete pointer;
        }
        pointer = ptr;
        ptr = nullptr;
    }

    void Swap(UniquePtr& other) {
        std::swap(pointer, other.pointer);
    }

    T* Get() const {
        return pointer;
    }
};


struct Item {
    static int counter;
    int value;
    Item(int v = 0) : value(v) {
        ++counter;
    }
    Item(const Item& other) : value(other.value) {
        ++counter;
    }
    ~Item() {
        --counter;
    }
};

int Item::counter = 0;


void TestLifetime() {
    Item::counter = 0;
    {
        UniquePtr<Item> ptr(new Item);
        ASSERT_EQUAL(Item::counter, 1);

        ptr.Reset(new Item);
        ASSERT_EQUAL(Item::counter, 1);
    }
    ASSERT_EQUAL(Item::counter, 0);

    {
        UniquePtr<Item> ptr(new Item);
        ASSERT_EQUAL(Item::counter, 1);

        auto rawPtr = ptr.Release();
        ASSERT_EQUAL(Item::counter, 1);

        delete rawPtr;
        ASSERT_EQUAL(Item::counter, 0);
    }
    ASSERT_EQUAL(Item::counter, 0);
}

void TestGetters() {
    UniquePtr<Item> ptr(new Item(42));
    ASSERT_EQUAL(ptr.Get()->value, 42);
    ASSERT_EQUAL((*ptr).value, 42);
    ASSERT_EQUAL(ptr->value, 42);
}

void TestMove() {
    UniquePtr<Item> ptr(new Item(42));
    auto ptr2(move(ptr));
}


int main() {
    TestRunner tr;
    RUN_TEST(tr, TestLifetime);
    RUN_TEST(tr, TestGetters);
    RUN_TEST(tr, TestMove);
}