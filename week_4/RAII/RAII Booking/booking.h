#pragma once
#include <memory>
namespace RAII {
	template <typename Provider>
	class Booking {
	private:
		Provider* _provider;
		//std::unique_ptr<Provider> _provider;
		int _counter;
	public:
		Booking() = default;
		Booking(const Booking& other) = delete;
		Booking(Booking&& other) :_provider(other._provider), _counter(other._counter) {
			other._provider = nullptr;
		}
		Booking(Provider* provider, const int counter) : _provider(provider), _counter(counter) {
			//provider = nullptr;
		}

		Booking& operator=(const Booking& other) = delete;
		Booking& operator=(Booking&& other) {
			if (other._provider != _provider) {
				_provider = other._provider;
				other._provider = nullptr;
				_counter = other._counter;
			}
			return *this;
		}

		~Booking() {
			if (_provider != nullptr) {
				_provider->CancelOrComplete(*this);
			}
		}
	};

}