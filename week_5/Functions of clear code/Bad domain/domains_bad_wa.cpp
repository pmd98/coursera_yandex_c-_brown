#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>
#include <string_view>
#include <vector>

using namespace std;

class DomainFilter {
private:
    std::vector<std::string> banned_domains;

public:
    DomainFilter(const std::vector <std::string>& banned_domains_) : banned_domains(banned_domains_){}
    DomainFilter(std::vector <std::string>&& banned_domains_) : banned_domains(std::move(banned_domains_)) {}

    bool IsSubdomain(std::string_view subdomain, std::string_view domain) {
        auto i = subdomain.size() - 1;
        auto j = domain.size() - 1;
        while (i > 0 && j > 0) {
            if (subdomain[i--] != domain[j--]) {
                return false;
            }
        }
        return (j == i) ||
            (domain[i] == '.' && domain[j] == '.') ||
            (j != 0 && domain[j - 1] == '.');
    }

    bool IsGoodDomain(const std::string& domain) {
        bool ban = true;
        for (const std::string& banned_domain : banned_domains) {
            if (IsSubdomain(banned_domain, domain))
            {
                ban = false;
                break;
            }
        }
        return ban;
    }
};

/*bool IsSubdomain(const string& subdomain,const string& domain) {
    size_t pos = domain.find(subdomain);
    if (pos != std::string::npos)
        return true;
    return false;
}

bool IsSubdomain(string_view subdomain, string_view domain) {
    auto i = subdomain.size() - 1;
    auto j = domain.size() - 1;
    while (i > 0 && j > 0) {
        if (subdomain[i--] != domain[j--]) {
            return false;
        }
    }
    return (j == i) || 
        (domain[i] == '.' && domain[j] == '.')||
        (j != 0 && domain[j - 1] == '.');
}*/

std::istream& operator>>(istream& is, std::vector<std::string>& domains) {
    size_t count;
    is >> count;
    is.ignore(1);
    string domain;
    for (size_t i = 0; i < count; ++i) {
        std::getline(is, domain);
        domains.emplace_back(domain);
    }
    return is;
}

int main() {
    
   /* const string input =
R"(2
c.b.
a.b.c
1
d.b.c)";
    
    const string input =
        R"(3
ya.ya
ya.ru
ya.com
6
haya.ya
teya.ru
suya.com
ha.ya.ya
te.ya.ru
su.ya.com)";*/
    const string input =
        R"(4
maps.me
m.ya.ru
com
ya.ru
7
ya.ru
ya.com
m.maps.me
moscow.m.ya.ru
maps.com
maps.ru
ya.ya)";
    istringstream stream(input);
    vector<string> banned_domains;
    stream >> banned_domains;
    DomainFilter filter(std::move(banned_domains));
    vector<string> domains_to_check;
    stream >> domains_to_check;
    bool ban = false;
   /* for (string& domain : banned_domains) {
        reverse(begin(domain), end(domain));
    }*/
    sort(begin(banned_domains), end(banned_domains));
    for (const string& domain : domains_to_check) {
        if (filter.IsGoodDomain(domain)) {
            cout << "Good\n";
        }
        else {
            cout << "Bad\n";
        }
    }
    return 0;
}

/*
    }*/