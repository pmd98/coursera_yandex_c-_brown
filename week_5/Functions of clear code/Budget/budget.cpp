#include<iostream>
#include<vector>
#include<map>
#include<iomanip>
#include<tuple>
#include<ctime>
#include<algorithm>
#include <sstream>

using namespace std;

std::istream& operator>>(std::istream& is, tm& date) {
    char bufChar;
    is >> date.tm_year >> bufChar >>
        date.tm_mon >> bufChar >>
        date.tm_mday;
    date.tm_mon--;
    date.tm_year -= 1900;
    date.tm_hour = date.tm_min = date.tm_sec = 0;
    return is;
}

std::ostream& operator<<(std::ostream& os, const tm& date) {
    os << date.tm_year + 1900 << "-" << date.tm_mon + 1 << "-" << date.tm_mday;
    return os;
}
/*
bool operator<(const tm& lhs, const tm& rhs)
{
    return std::make_tuple(lhs.tm_year, lhs.tm_mon, lhs.tm_mday) <
        std::make_tuple(rhs.tm_year, rhs.tm_mon, rhs.tm_mday);
}

bool operator>(const tm& lhs, const tm& rhs)
{
    return std::make_tuple(lhs.tm_year, lhs.tm_mon, lhs.tm_mday) >
        std::make_tuple(rhs.tm_year, rhs.tm_mon, rhs.tm_mday);
}

bool operator<=(const tm& lhs, const tm& rhs)
{
    return std::make_tuple(lhs.tm_year, lhs.tm_mon, lhs.tm_mday) <=
        std::make_tuple(rhs.tm_year, rhs.tm_mon, rhs.tm_mday);
}

bool operator>=(const tm& lhs, const tm& rhs)
{
    return std::make_tuple(lhs.tm_year, lhs.tm_mon, lhs.tm_mday) >=
        std::make_tuple(rhs.tm_year, rhs.tm_mon, rhs.tm_mday);
}

*/
size_t calcDuration(const tm& from, const  tm& to) {
    tm _to(to), _from(from);
    const time_t timestamp_to = mktime(&_to);
    const time_t timestamp_from = mktime(&_from);
    static constexpr int SECONDS_IN_DAY = 60 * 60 * 24;
    return ((timestamp_to - timestamp_from) / SECONDS_IN_DAY )+ 1;
    //return difftime(mktime(&_to), mktime(&_from)) / (86'400) + 1;
}
/*
class DateRange {
private:
    tm start, finish;
    size_t duration;
public:
    explicit DateRange(const tm& from, const tm& to) : start(from), finish(to) {
        duration = difftime(mktime(&finish), mktime(&start)) / (86'400) + 1;
    }

    const tm& getFinish() const { return finish; }

    const tm& getStart() const { return start; }

    bool withinRange(const tm& date)const {
        return start <= date && date <= finish;
    }

    size_t getDuration() const {
        return duration;
    }
};
*/


class Budget {
public:
    Budget() { days.resize(36525); 
    zero_day.tm_year = 100;
    zero_day.tm_mon = zero_day.tm_mday =
        zero_day.tm_hour = zero_day.tm_min = zero_day.tm_sec = 0;
    }

    double ComputeIncome(const tm& from, const tm& to)const {
        size_t shift = calcDuration(zero_day, from);
        size_t imcome_period = calcDuration(from, to);
        double result = 0.0;
        for (size_t i = shift; i < shift + imcome_period; ++i) {
            result += days[i].income - days[i].spend;
        }
        return result;
    }

    void Earn(const tm& from, const tm& to, size_t value) {
        size_t shift = calcDuration(zero_day, from);
        size_t imcome_period = calcDuration(from, to);
        double income_per_day = (value * 1.0) / imcome_period;
        for (size_t i = shift; i < shift + imcome_period; ++i) {
            days[i].income += income_per_day;
        }
    }

    void Spend(const tm& from, const tm& to, size_t value) {
        size_t shift = calcDuration(zero_day, from);
        size_t imcome_period = calcDuration(from, to);
        double spend_per_day = (value * 1.0) / imcome_period;
        for (size_t i = shift; i < shift + imcome_period; ++i) {
            days[i].spend += spend_per_day;
        }
    }

    void PayTax(const tm& from, const tm& to, size_t percentage) {
        size_t shift = calcDuration(zero_day, from);
        size_t imcome_period = calcDuration(from, to);
        for (size_t i = shift; i < shift + imcome_period; ++i) {
            days[i].income *= 1.0 - (percentage/100.0);
        }
    }

private:
    struct DayIncome
    {
        double income;
        double spend;
    };
    std::vector<DayIncome> days;
    tm zero_day;
};

/*const string input(R"(8
Earn 2000-01-02 2000-01-06 20
ComputeIncome 2000-01-01 2001-01-01
PayTax 2000-01-02 2000-01-03
ComputeIncome 2000-01-01 2001-01-01
Earn 2000-01-03 2000-01-03 10
ComputeIncome 2000-01-01 2001-01-01
PayTax 2000-01-03 2000-01-03
ComputeIncome 2000-01-01 2001-01-01)");

const string input(R"(8
Earn 2000-01-02 2000-01-06 20
ComputeIncome 2000-01-01 2001-01-01
PayTax 2000-01-02 2000-01-03 13
ComputeIncome 2000-01-01 2001-01-01
Spend 2000-12-30 2001-01-02 14
ComputeIncome 2000-01-01 2001-01-01
PayTax 2000-12-30 2000-12-30 13
ComputeIncome 2000-01-01 2001-01-01)");*/

int main() {
    //istringstream stream(input);
    Budget budget;
    tm from, to;
    std::string command;
    size_t N;
    //stream >> N;
    std::cin >> N;
    //std::cerr << N << "\n";
    for (size_t i = 0; i < N; ++i) {
        std::cin >> command >> from >> to;
        //std::cerr << "Command: " << command << "\nFrom: " << from << "\nTo: " << to << "\n";
        //stream >> command >> from >> to;
        if (command == "ComputeIncome")
        {
            std::cout << setprecision(25) << budget.ComputeIncome(from, to) << "\n";
        }
        else if (command == "Earn")
        {
            size_t value;
            std::cin >> value;
            //stream >> value;
            //std::cerr << "Value: " << value << "\n";
            budget.Earn(from, to, value);
        }
        else if (command == "PayTax")
        {
            size_t percentage;
            std::cin >> percentage;
            //stream >> percentage;
            budget.PayTax(from, to, percentage);
        }
        else if (command == "Spend") {
            size_t value;
            std::cin >> value;
            //stream >> value;
            budget.Spend(from, to, value);
        }
        else {
            cerr << "Wrong command!\n";
        }
    }

    return 0;
}